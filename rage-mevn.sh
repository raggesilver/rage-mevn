#!/bin/bash

if [[ $# -le 0 ]]; then
    echo "Too few arguments"
    exit 1
fi

name=$1

if [[ ! -e $name ]]; then
    mkdir -p $name
fi

cd $name

# Initializing git repo
git init

# Download gitignore
wget -O .gitignore https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore

# Writing package.json
read -r -d '' package << EOM
{
    "name": "$name",
    "version": "1.0.0",
    "description": "",
    "main": "app.js",
    "scripts": {
        "start": "node app"
    },
    "author": {
        "email": "$(git config user.email)",
        "name": "$(git config user.name)"
    },
    "license": "MIT"
}
EOM

echo -n $package > package.json

# Dependencies
declare -a deps=("express"
                 "passport"
                 "passport-local"
                 "mongoose"
                 "cors"
                 "colog"
                 "bcrypt")

function join { local IFS="$1"; shift; echo "$*"; }

deps_str=$(join " " ${deps[@]})
npm i $deps_str --save

# Installing vue
npm i vue-cli --save
./node_modules/vue-cli/bin/vue init webpack vue-src

# Folders
declare -a folders=("modules"
                    "models"
                    "config"
                    "public/dist"
                    "routes")

folders_str=$(join " " ${folders[@]})
mkdir -p $folders_str

# Getting app.js
wget -O app.js https://gitlab.com/snippets/1730649/raw

# Getting router.js
wget -O modules/router.js https://gitlab.com/snippets/1730654/raw